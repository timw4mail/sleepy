<?php

use Sleepy\Core\Interfaces\Type as iType;
use Sleepy\Core\Abstracts\Type as aType;

class aTypeTest extends Sleepy_TestCase {
	
	public function testSanity()
	{
		$this->assertEquals(
			['Sleepy\\Core\\Interfaces\\Type' => 'Sleepy\\Core\\Interfaces\\Type'],
			class_implements('Sleepy\\Core\\Abstracts\\Type')
		);
	}
	
	

}
// End of aTypeTest.php