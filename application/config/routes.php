<?php
/**
 * Sleepy - a REST framework
 *
 * 
 * A PHP Rest Framework valuing convention over configuration,
 * but aiming to be as flexible as possible
 *
 * @author Timothy J. Warren
 * @package Sleepy
 */
 
return [

	// --------------------------------------------------------------------------
	// The index controller, called if not specified in the routes or url
	// --------------------------------------------------------------------------
	'default_controller' => 'index',
	
];

// End of config/routes.php