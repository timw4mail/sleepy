<?php

use Sleepy\Core\Input;

class InputTest extends Sleepy_TestCase {

	public function setUp()
	{
		parent::setUp();

		// Set some http headers as test items
		$_SERVER['HTTP_USER_AGENT'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36 OPR/18.0.1284.49';
		$_SERVER['HTTP_ACCEPT'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';
		$_SERVER['HTTP_COOKIE'] = 'thp_tcms_session_id=9h4nvk15tjjegbeg8uvejncc9blkd81m';
		$_SERVER['HTTP_HOST'] = 'www.example.com';
		$_SERVER['HTTP_FOO'] = 'foo,bar';

		// Set test $_COOKIE array
		$_COOKIE = [
			'thp_tcms_session_id' => '9h4nvk15tjjegbeg8uvejncc9blkd81m'
		];

		// Set test $_GET array
		$_GET = [
			'foo' => 'bar',
			'baz' => 'foobar'
		];

		// Set test $_POST array
		$_POST = [
			'this' => 'that',
			'tisket' => 'tasket'
		];

		$this->input = new Input();
	}

	public function dataTestHeader()
	{
		return [
			'all' => [
				'index' => NULL,
				'expected' => [
					'user-agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36 OPR/18.0.1284.49',
					'accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
					'cookie' => 'thp_tcms_session_id=9h4nvk15tjjegbeg8uvejncc9blkd81m',
					'host' => 'www.example.com',
					'foo' => 'foo,bar'
				]
			],
			'index' => [
				'index' => 'cookie',
				'expected' => 'thp_tcms_session_id=9h4nvk15tjjegbeg8uvejncc9blkd81m'
			],
			'invalid' => [
				'index' => 'boo',
				'expected' => NULL
			]
		];
	}

	public function dataHeaderArray()
	{
		return [
			'all' => [
				'index' => NULL,
				'expected' => [
					'user-agent' => [
						'versions' => [
							'Mozilla' => '5.0',
							'AppleWebKit' => '537.36',
							'Chrome' => '31.0.1650.57',
							'Safari' => '537.36',
							'OPR' => '18.0.1284.49',
						],
						'os' => [
							'Macintosh',
							'Intel Mac OS X 10_9_0',
						],
						'misc' => 'KHTML, like Gecko'
					],
					'accept' => [
						'0.8' => '*/*',
						'0.9' => 'application/xml',
						1 => 'application/xhtml+xml',
						2 => 'text/html'
					],
					'cookie' => [
						'thp_tcms_session_id' => '9h4nvk15tjjegbeg8uvejncc9blkd81m'
					],
					'host' => 'www.example.com',
					'foo' => [
						'foo',
						'bar'
					]
				]
			],
			'index' => [
				'index' => 'cookie',
				'expected' => [
					'thp_tcms_session_id' => '9h4nvk15tjjegbeg8uvejncc9blkd81m'
				]
			],
			'invalid' => [
				'index' => 'red',
				'expected' => NULL
			]
		];
	}

	public function dataTestPost()
	{
		return [
			'all' => [
				'index' => NULL,
				'expected' => [
					'this' => 'that',
					'tisket' => 'tasket'
				]
			],
			'index' => [
				'index' => 'this',
				'expected' => 'that'
			],
			'invalid' => [
				'index' => 'puppies',
				'expected' => NULL
			]
		];
	}

	public function dataTestGet()
	{
		return [
			'all' => [
				'index' => NULL,
				'expected' => [
					'foo' => 'bar',
					'baz' => 'foobar'
				]
			],
			'index' => [
				'index' => 'foo',
				'expected' => 'bar'
			],
			'invalid' => [
				'index' => 'applesauce',
				'expected' => NULL
			]
		];
	}

	/**
	 * @dataProvider dataTestHeader
	 */
	public function testHeader($index, $expected)
	{
		$res = $this->input->header($index);
		$this->assertEquals($expected, $res);
	}

	/**
	 * @dataProvider dataHeaderArray
	 */
	public function testHeaderArray($index, $expected)
	{
		$result = $this->input->header_array($index);
		$this->assertEquals($expected, $result);
	}

	/**
	 * @dataProvider dataTestPost
	 */
	public function testPost($index, $expected)
	{
		$result = $this->input->post($index);
		$this->assertEquals($expected, $result);
	}

	/**
	 * @dataProvider dataTestGet
	 */
	public function testGet($index, $expected)
	{
		$result = $this->input->get($index);
		$this->assertEquals($expected, $result);
	}
	
	public function testBadVar()
	{
		try {
			$foo = $this->input->applesauce();
		}
		catch (DomainException $e) {
			$this->assertTrue(true);
		}
	}
}
// End of InputTest.php