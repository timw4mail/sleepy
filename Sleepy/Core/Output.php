<?php
/**
 * Sleepy - a REST framework
 *
 *
 * A PHP Rest Framework valuing convention over configuration,
 * but aiming to be as flexible as possible
 *
 * @author Timothy J. Warren
 */

namespace Sleepy\Core;

/**
 * Default output class
 */
class Output {

	/**
	 * The data to serialize and output
	 *
	 * @var mixed
	 */
	protected $data;

	/**
	 * A list of HTTP headers to send with a response
	 *
	 * @var array
	 */
	protected $headers = [];

	/**
	 * The serialization object for the current data type
	 *
	 * @var Sleepy\Core\aType
	 */
	protected $type_wrapper;

	/**
	 * The input object
	 *
	 * @var Sleepy\Core\Input
	 */
	protected $input;
	
	/**
	 * Config object
	 * 
	 * @var Sleepy\Core\Config;
	 */
	protected $config;

	// --------------------------------------------------------------------------
	// ! Methods
	// --------------------------------------------------------------------------

	/**
	 * Create the output object
	 *
	 * @param Config $config
	 * @param Input $input
	 */
	public function __construct(Config $config, Input $input)
	{
		$this->config = $config;
		$this->input = $input;
	}

	/**
	 * Output the data to the client
	 * @codeCoverageIgnore
	 */
	public function __destruct()
	{
		// Output the headers
		$this->_output_headers();

		// Echo the response
		echo $this->type_wrapper;
	}

	/**
	 * Add a header to be output
	 *
	 * @param mixed $header
	 * @return void
	 */
	public function set_header($header)
	{
		if (is_array($header))
		{
			foreach($header as $type => $val)
			{
				$this->headers[] =  (is_numeric($type))
					? $val
					: "{$type}: {$val}";
			}
		}
		else
		{
			$this->headers[] = $header;
		}
	}

	/**
	 * Set the data to be output to the endpoint
	 *
	 * @param mixed $type - The data format to send
	 * @param mixed $data - The data to send
	 * @return void
	 */
	public function set_data($type = 'html', $data = NULL)
	{
		if (is_null($data) && ! empty($this->data))
		{
			$data = $this->data;
		}

		// Set instance data
		$this->data = $data;

		// Get the appropriate output format for the client
		// And set the data
		return $this->get_accepted_type($type, $this->data);
	}

	// --------------------------------------------------------------------------
	// ! Private helper methods
	// --------------------------------------------------------------------------

	/**
	 * Get the type more accepted for output
	 *
	 * @param mixed $types
	 * @param mixed $data
	 * @return array
	 */
	protected function get_accepted_type($types, $data)
	{
		// Get the mime - type class mapping to use
		// for determining which classes need to be loaded
		$types = (array) $types;
		$types = array_map('strtoupper', $types);
		$type_map = $this->config->get('type_class_map');
		$filtered_type_map = $this->filter_mime_types($type_map, $types);

		// Get the accept headers to filter valid mime types 
		// for the current client
		$headers = $this->input->header_array();
		$accept = array_flip($headers['accept']);
		$valid_mimes = array_keys(array_intersect_key($accept, $filtered_type_map));
		
		// When you don't have a matching mime, send the default 
		// data type specified for the output in the type_class_map
		// config file
		if (empty($valid_mimes))
		{
			$valid_mimes[] = $type_map['*/*'];
		}

		// Map type objects to the appropriate 
		// associated mime types
		$classes = [];
		foreach($valid_mimes as $mime)
		{
			$t = $type_map[$mime];
			$type_class = "Sleepy\\Type\\{$t}";
			$classes[$mime] = $type_class;
		}

		// Use the first output type to output the data
		$selected_mime = array_shift($valid_mimes);
		$class = $classes[$selected_mime];
		$this->type_wrapper = new $class($data);

		// Make sure to set the content-type header
		if (empty($this->headers))
		{
			$mime = $this->type_wrapper->get_mime();
			$this->set_header("Content-type: {$mime};charset=utf8");
		}
		
		return [$selected_mime => $class];
	}
	
	/**
	 * Filter the list of mime types by their values
	 * 
	 * @param array $mime_list
	 * @param array $type_list
	 * @return array
	 */
	protected function filter_mime_types($mime_list, $type_list)
	{
		$filtered_list = [];
		
		foreach($type_list as $class)
		{
			foreach($mime_list as $mime => $c)
			{
				if (strtoupper($c) === strtoupper($class))
				{
					$filtered_list[$mime] = $c;
				}
			}
		}
		
		return $filtered_list;
	}

	/**
	 * Set the applicable response headers
	 *
	 * @codeCoverageIgnore
	 * @return void
	 */
	protected function _output_headers()
	{
		foreach($this->headers as $header)
		{
			@header($header);
		}
	}
}
// End of Core/Output.php