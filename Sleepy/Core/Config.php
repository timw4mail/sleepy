<?php
/**
 * Sleepy - a REST framework
 *
 *
 * A PHP Rest Framework valuing convention over configuration,
 * but aiming to be as flexible as possible
 *
 * @author Timothy J. Warren
 */

namespace Sleepy\Core;

/**
 * Class for managing configuration values
 */
class Config {
	
	/**
	 * The config array
	 * @var array
	 */
	protected $data = [];
	
	/**
	 * Load the data into the class member
	 */
	public function __construct()
	{
		$conf_files = glob(APPPATH . 'config/*.php');
		
		foreach($conf_files as $file)
		{
			$data = require_once($file);
			
			$name = str_replace('.php', '', basename($file));
			$this->data[$name] = $data;
		}
	}

	
	/**
	 * Get the specific parameter from the specified file
	 * 
	 * @throws \InvalidArgumentException
	 * @param string $file
	 * @param string $key
	 * @return mixed
	 */
	public function get($file, $key=NULL)
	{
		if ( ! array_key_exists($file, $this->data))
		{
			throw new \InvalidArgumentException("The config file doesn't exist");
		}
		
		return (is_null($key))
			? $this->data[$file]
			: $this->data[$file][$key];
	}
}
// End of Core/Config.php