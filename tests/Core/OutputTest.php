<?php

use Sleepy\Core\Input;
use Sleepy\Core\Config;
use Sleepy\Core\Output;

class OutputTest extends Sleepy_Testcase {
	
	protected $output;
	
	public function setUp()
	{
		parent::setUp();
		
		$c = new Config();
		$i = new Input();
		
		$this->output = new MockOutput($c, $i);
	}
	
	public function dataSetData()
	{
		return [
			'default mime' => [
				'accept' => 'text/csv, text/plain;q=0.8, */*;q=0.1',
				'parsed_accept' => [
					'1' => 'text/csv',
					'0.8' => 'text/plain',
					'0.1' => '*/*'
				],
				'types' => 'json',
				'mapping' => [
					'text/xml' => 'XML',
					'foo/bar' => 'HTML',
					'application/json' => 'JSON',
					'*/*' => 'application/json'
				],
				'expected' => [
					'application/json' => 'Sleepy\Type\JSON'
				]
			],
			'html' => [
				'accept' => 'text/html, text/plain;q=0.5',
				'parsed_accept' => [
					'1' => 'text/html',
					'0.5' => 'text/plain'
				],
				'types' => ['json', 'xml', 'html'],
				'mapping' => [
					'text/html' => 'HTML',
					'application/json' => 'JSON',
					'text/plain' => 'YAML'
				],
				'expected' => [
					'text/html' => 'Sleepy\Type\HTML'
				]
			]
		];
	}
	
	/**
	 * @dataProvider dataSetData
	 */
	public function testSetData($accept, $parsed_accept, $types, $mapping, $expected)
	{
		$_SERVER['HTTP_ACCEPT'] = $accept;
		
		// Recreate input object to use new 
		// superglobal value
		$in = new Input();
		$pheaders = $in->header_array();
		$this->output->setInput($in);
		
		// Sanity check of accept header
		$this->assertEquals($parsed_accept, $pheaders['accept']);
		
		// Mock config to set the mime/class mapping list
		$conf = new MockConfig();
		$conf->setData(['type_class_map' => $mapping]);
		$this->output->setConfig($conf);
		
		$this->output->setData(['foo' => 'bar']);
		$res = $this->output->set_data($types);
		
		$this->assertEquals($expected, $res);
	}
	
	public function dataSetHeader()
	{
		return [
			'single_header' => [
				'input' => 'Content-type: text/html',
				'expected' => [
					'Content-type: text/html'
				]
			],
			'header_array' => [
				'input' => [
					'Content-type: text/plain',
					'Accept-encoding' =>  'utf-8'
				],
				'expected' => [
					'Content-type: text/plain',
					'Accept-encoding: utf-8'
				]
			]
		];
	}
	
	/**
	 * @dataProvider dataSetHeader
	 */
	public function testSetHeader($input, $expected)
	{
		$this->output->set_header($input);
		$headers = $this->output->getHeaders();
		
		$this->assertEquals($expected, $headers);
	}
}
// End of OutputTest.php