<?php
/**
 * Sleepy - a REST framework
 *
 *
 * A PHP Rest Framework valuing convention over configuration,
 * but aiming to be as flexible as possible
 *
 * @author Timothy J. Warren
 * @package Sleepy
 */

namespace Sleepy;

define('BASEPATH', __DIR__ . '/');
define('SLEEPY_DIR', 'Sleepy');
define('APP_DIR', 'application');
define('APPPATH', __DIR__ . '/application/');

// Autoloaders
include BASEPATH . 'vendor/autoload.php';
require BASEPATH . SLEEPY_DIR . '/autoload.php';

// And...bootstrap
require APPPATH . 'bootstrap.php';

// End of index.php