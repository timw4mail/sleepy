<?php
/**
 * Sleepy - a REST framework
 *
 * 
 * A PHP Rest Framework valuing convention over configuration,
 * but aiming to be as flexible as possible
 *
 * @author Timothy J. Warren
 * @package Sleepy
 */
 
return [

	// --------------------------------------------------------------------------
	// Map mime types to type classes
	// --------------------------------------------------------------------------
	'application/json' 		=> 'JSON',
	'text/yaml' 			=> 'YAML',
	'application/yaml' 		=> 'YAML',
	'text/html' 			=> 'HTML',
	'text/xml'				=> 'XML',
	'application/xml'		=> 'XML',
	
	// Default mime type for cases without an explicit accept header match 
	'*/*'					=> 'text/html'
];

// End of config/type_class_map.php