<?php 

use Sleepy\Type\XML;

class MockXML extends XML {

	protected $mime = '';
}

class XMLTest extends Sleepy_Testcase {

	public function setUp() {
		$test_array = [
			'foo' => 'bar',
			'baz' => [
				0 => ['x' => 'y']
			]
		];
		
		$this->XML = new XML($test_array);
	}

	public function testSanity() {
		$this->assertTrue(is_a($this->XML, 'Sleepy\\Type\\XML'));
		$this->assertTrue(is_a($this->XML, 'Sleepy\\Core\\Abstracts\\Type'));
		$this->assertEquals(
			['Sleepy\\Core\\Interfaces\\Type' => 'Sleepy\\Core\\Interfaces\\Type'],
			class_implements('Sleepy\\Type\\XML')
		);
	}

	public function testNIE()  {
		try {
			$xml = new MockXML([]);
		}
		catch (Sleepy\Exception\NotImplementedException $e) {
			$this->assertTrue(TRUE);
		}
	}
	
	public function testSerialize()
	{
		$xml = $this->XML->serialize();
		$xml_tostring = $this->XML->__toString();
		$expected = '<?xml version="1.0"?>' . 
			"\n<root><foo>bar</foo><baz><item_0><x>y</x></item_0></baz></root>\n";
		
		$this->assertEquals($expected, $xml);
		$this->assertEquals($xml, $xml_tostring);
	}
	
	public function testUnSerialize()
	{
		$object = $this->XML->unserialize('<?xml version="1.0"?>' . "\n<root><foo>bar</foo></root>\n");
		
		$expected = (object) ['foo' => 'bar'];
		
		$this->assertEquals($expected, $object);
	}

}
// End of XMLTest