<?php
/**
 * Sleepy - a REST framework
 *
 *
 * A PHP Rest Framework valuing convention over configuration,
 * but aiming to be as flexible as possible
 *
 * @author Timothy J. Warren
 */

namespace Sleepy\Type;

use \Sleepy\Core\Abstracts\Type as aType;

/**
 * Class for JSON output
 */
class JSON extends aType {

	/**
	 * The mime type for output
	 *
	 * @var string
	 */
	protected $mime = 'application/json';

	/**
	 * Convert the data into the output format
	 *
	 * @param mixed $data
	 * @return string
	 */
	public function serialize($data = NULL)
	{
		$this->set_data($data);
		return json_encode($data, JSON_PRETTY_PRINT);
	}

	/**
	 * Convert the encoded data to a native format
	 *
	 * @param string $data_string
	 * @return object
	 */
	public function unserialize($data_string)
	{
		return json_decode($data_string);
	}

}

// End of types/JSON.php