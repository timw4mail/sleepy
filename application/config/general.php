<?php
/**
 * Sleepy - a REST framework
 *
 * 
 * A PHP Rest Framework valuing convention over configuration,
 * but aiming to be as flexible as possible
 *
 * @author Timothy J. Warren
 * @package Sleepy
 */
 
return [

	// --------------------------------------------------------------------------
	// The default format of data to be sent. Can be overwritten in controllers
	// --------------------------------------------------------------------------
	'default_output_format' => 'json',
	
	// --------------------------------------------------------------------------
	// The default format of data recieved. Can be form data or one of the 
	// types defined in either the application/types or Sleepy/types folders
	// --------------------------------------------------------------------------
	'default_input_format' => 'json',

];

// End of config/general.php