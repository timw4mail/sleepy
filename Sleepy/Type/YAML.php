<?php
/**
 * Sleepy - a REST framework
 *
 *
 * A PHP Rest Framework valuing convention over configuration,
 * but aiming to be as flexible as possible
 *
 * @author Timothy J. Warren
 */

namespace Sleepy\Type;

use \Sleepy\Core\Abstracts\Type as aType;
use Symfony\Component\Yaml\Yaml as YML;

/**
 * Class for YAML output
 */
class YAML extends aType {

	/**
	 * The mime type for output
	 *
	 * @var string
	 */
	protected $mime = 'text/yaml';

	/**
	 * Convert the data into the output format
	 *
	 * @param mixed $data
	 * @return string
	 */
	public function serialize($data = null)
	{
		$this->set_data($data);

		// Yaml class doesn't support objects, cast them to arrays
		$data = (array) $data;

		return YML::dump($data);
	}

	/**
	 * Convert the encoded data to a native format
	 *
	 * @param string $data_string
	 * @return object
	 */
	public function unserialize($data_string)
	{
		return YML::parse($data_string, FALSE, TRUE);
	}
}
// End of types/YAML.php