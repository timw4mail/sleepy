<?php
/**
 * Sleepy - a REST framework
 *
 *
 * A PHP Rest Framework valuing convention over configuration,
 * but aiming to be as flexible as possible
 *
 * @author Timothy J. Warren
 * @package Sleepy
 */

namespace Sleepy;

/**
 * Autoloader
 */
\spl_autoload_register(function($item) {
	$path_items = \explode('\\', \ltrim($item, '\\'));

	// If the namespace is a straight mapping to the class, just load it
	$simple_path = \implode('/', $path_items);
	$file = BASEPATH . "{$simple_path}.php";

	if (file_exists($file))
	{
		require_once($file);
		return;
	}

	// Check the application folder
	$file = str_replace(SLEEPY_DIR, APP_DIR, $file);
	if (file_exists($file))
	{
		require_once($file);
		return;
	}
});

// End of autoload.php