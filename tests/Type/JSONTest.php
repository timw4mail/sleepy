<?php

use Sleepy\Type\JSON;

class MockJSON extends JSON {

	protected $mime = '';
}

class JSONTest extends Sleepy_Testcase {

	public function setUp() {
		$this->JSON = new JSON(['foo'=>'bar']);
	}

	public function testSanity() {
		$this->assertTrue(is_a($this->JSON, 'Sleepy\\Type\\JSON'));
		$this->assertTrue(is_a($this->JSON, 'Sleepy\\Core\\Abstracts\\Type'));
		$this->assertEquals(
			['Sleepy\\Core\\Interfaces\\Type' => 'Sleepy\\Core\\Interfaces\\Type'],
			class_implements('Sleepy\\Type\\JSON')
		);
	}

	public function testNIE()  {
		try {
			$json = new MockJSON([]);
		}
		catch (Sleepy\Exception\NotImplementedException $e) {
			$this->assertTrue(TRUE);
		}
	}
	
	public function testGetMime()
	{
		$mime = $this->JSON->get_mime();
		$this->assertEquals('application/json', $mime);
	}
}
// End of JSONTest