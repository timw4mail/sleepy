<?php
/**
 * Sleepy - a REST framework
 *
 *
 * A PHP Rest Framework valuing convention over configuration,
 * but aiming to be as flexible as possible
 *
 * @author Timothy J. Warren
 */

namespace Sleepy\Core\Abstracts;

use Sleepy\Core\Interfaces\Type as iType;
use Sleepy\Exception\NotImplementedException;

/**
 * Abstract class with helpful functionality implementing type functionality
 */
abstract class Type implements iType {

	/**
	 * The data in the current type wrapper
	 *
	 * @var mixed
	 */
	protected $data;

	/**
	 * The mime type to output for the current type
	 *
	 * @var string
	 */
	protected $mime;

	/**
	 * Create the type object with the specified data
	 *
	 * @param mixed $data
	 * @throws NotImplementedException
	 */
	public function __construct($data = NULL)
	{
		if (empty($this->mime))
		{
			throw new NotImplementedException("Output types must have a mime type defined.");
		}

		$this->set_data($data);
	}
	
	/**
	 * Set data to serialize
	 * 
	 * @param mixed $data
	 * @return mixed
	 */
	protected function set_data(&$data)
	{
		if ( ! is_null($data))
		{
			$this->data = $data;
		}
		else
		{
			$data = $this->data;
		}
		
		return $data;
	}

	/**
	 * Returns the mime type needed
	 * for the current type
	 *
	 * @return string
	 */
	public function get_mime()
	{
		return $this->mime;
	}

	/**
	 * Output the data as a string
	 *
	 * @return string
	 */
	public function __toString()
	{
		return $this->serialize($this->data);
	}

}
// End of Core/Abstracts/Type.php