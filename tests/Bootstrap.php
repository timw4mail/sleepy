<?php

// Use the same autoloader as the app for consistency
define('BASEPATH', '../');
define('SLEEPY_DIR', 'Sleepy');
define('APP_DIR', 'application');
define('APPPATH', BASEPATH . 'application/');
include BASEPATH . '/vendor/autoload.php';
require BASEPATH . SLEEPY_DIR . '/autoload.php';

/**
 * Base testcase class
 */
class Sleepy_TestCase extends PHPUnit_Framework_TestCase {

	public function setUp()
	{
		if ( ! isset($_SERVER['REQUEST_METHOD']))
		{
			$_SERVER['REQUEST_METHOD'] = 'GET';
		}
	}
}

// --------------------------------------------------------------------------
// ! Simple Base Mocks
// --------------------------------------------------------------------------

use Sleepy\Core\Output;
use Sleepy\Core\Config;

class MockOutput extends Output {
	use Sleepy\Traits\getSet;

	public function __destruct()
	{
		$this->_output_headers();
	}
}

class MockConfig extends Config {
	use Sleepy\Traits\getSet;
}

// End of tests/Bootstrap.php