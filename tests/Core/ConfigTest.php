<?php

use Sleepy\Core\Config;

class ConfigTest extends Sleepy_Testcase {
	
	public function setUp()
	{
		parent::setUp();
		$this->config = new MockConfig();
		$this->config->setData([
			'foo' => [
				'bar' => 'baz',
				'x' => '-y',
				'p !=' => 'np'	
			],
			'apple' => [1,3,5,7,9]
		]);
	}
	
	public function dataGet()
	{
		return [
			'single value' => [
				'file' => 'foo',
				'key' => 'x',
				'expected' => '-y'
			],
			'whole array' => [
				'file' => 'apple',
				'key' => NULL,
				'expected' => [1,3,5,7,9]
			]
		];
	}
	
	/**
	 * @dataProvider dataGet
	 */
	public function testGet($file, $key, $expected)
	{
		$res = $this->config->get($file, $key);
		$this->assertEquals($expected, $res);
	}
	
	
	public function testBadGet()
	{
		try 
		{
			$this->config->get('bleucheese');
		}
		catch (\InvalidArgumentException $e)
		{
			$this->assertTrue(TRUE, "Proper exception was caught");
		}
	}
}

// End of ConfigTest.php