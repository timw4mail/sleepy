<?php
/**
 * Sleepy - a REST framework
 *
 *
 * A PHP Rest Framework valuing convention over configuration,
 * but aiming to be as flexible as possible
 *
 * @author Timothy J. Warren
 */

namespace Sleepy\Traits;

/**
 * Trait to get rid of naive getter/setter boilerplate
 */
trait GetSet 
{

	/**
	 * Dynamically create getters/setters
	 *
	 * @param string $func
	 * @param array $args
	 * @return mixed
	 */
	public function __call($func, $args)
	{
		$type = substr($func, 0, 3);
		$val = strtolower(substr($func, 3));
		$valid_types = array(
			'get' => 'get',
			'set' => 'set'
		);
		
		if ( ! property_exists($this, $val) || ! isset($valid_types[$type])) 
		{
			return NULL;
		}
		
		// Auto-magical getters and setters
		if ($type === 'get') 
		{
			return $this->$val;
		} 
		elseif ($type === 'set') 
		{
			$this->$val = current($args);
		}
	}
}
// End of GetSet.php