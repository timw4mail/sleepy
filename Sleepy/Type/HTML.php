<?php
/**
 * Sleepy - a REST framework
 *
 *
 * A PHP Rest Framework valuing convention over configuration,
 * but aiming to be as flexible as possible
 *
 * @author Timothy J. Warren
 */

namespace Sleepy\Type;

use \Sleepy\Core\Abstracts\Type as aType;

/**
 * Class for HTML output
 */
class HTML extends aType {

	/**
	 * The mime type for output
	 *
	 * @var string
	 */
	protected $mime = 'text/html';

	/**
	 * Convert the data into the output format
	 *
	 * @param mixed $data
	 * @return string
	 */
	public function serialize($data = NULL)
	{
		$this->set_data($data);

		if (is_string($data)) return $data;

		// Lets use JSON as an output format if the value isn't scalar
		return '<pre>' . json_encode($data, JSON_PRETTY_PRINT) . '</pre>';
	}

	/**
	 * Convert the encoded data to a native format
	 *
	 * @param string $data_string
	 * @return object
	 */
	public function unserialize($data_string)
	{
		return NULL;
	}

}

// End of types/JSON.php