<?php
/**
 * Sleepy - a REST framework
 *
 *
 * A PHP Rest Framework valuing convention over configuration,
 * but aiming to be as flexible as possible
 *
 * @author Timothy J. Warren
 * @package Sleepy
 */

namespace Sleepy\Core;

// Include namespaces
use Aura\Di\Container as DiContainer;
use Aura\Di\Factory as DiFactory;
use Aura\Router\RouterFactory;

$di = new DiContainer(new DiFactory());

$di->set('config', $di->newInstance('Sleepy\Core\Config'));
$di->set('router', $di->newInstance('Aura\Router\RouterFactory')->newInstance());
$di->set('input', $di->newInstance('Sleepy\Core\Input'));

$di->params['Sleepy\Core\Output'] = [
	'config' => $di->get('config'),
	'input' => $di->get('input')
];
$di->set('output', function () use ($di) {
	return $di->newInstance('Sleepy\Core\Output');
});

// Set the output data
$browser = get_browser();
$browser->browser_name_regex = \utf8_encode($browser->browser_name_regex);
$i = $di->get('input');
$di->get('output')->set_data(['json','yaml'], [
	'$_SERVER' => $i->server(),
	'$_GET' => $i->get(),
	'$_POST' => $i->post(),
	'$_PUT' => $i->put(),
	'$_DELETE' => $i->delete(),
	'$_ENV' => $i->env(),
	'$_COOKIE' => $i->cookie(),
	'browser' => $browser,
	'raw headers' => $i->header(),
	'parsed headers' => $i->header_array()
]);

// End of bootstrap.php