<?php
/**
 * Sleepy - a REST framework
 *
 *
 * A PHP Rest Framework valuing convention over configuration,
 * but aiming to be as flexible as possible
 *
 * @author Timothy J. Warren
 */

namespace Sleepy\Exception;

/**
 * Exception for enforcing properties that need to be defined,
 * or covering methods that are not yet implemented.
 */
class NotImplementedException extends \LogicException {

	/**
	 * Have a default message for the exception
	 *
	 * @param string $message - The exception message
	 */
	public function __construct($message = "Hmm...you stumbled onto something that is not implemented.")
	{
		parent::__construct($message);
	}

};

// End of exceptions/NotImplementedException.php