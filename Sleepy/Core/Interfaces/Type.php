<?php
/**
 * Sleepy - a REST framework
 *
 *
 * A PHP Rest Framework valuing convention over configuration,
 * but aiming to be as flexible as possible
 *
 * @author Timothy J. Warren
 */

namespace Sleepy\Core\Interfaces;

/**
 * Interface for output formats
 */
interface Type {

	/**
	 * Convert the data to the output format
	 *
	 * @param mixed $data
	 * @return string
	 */
	public function serialize($data);

	/**
	 * Convert the format to php data
	 *
	 * @param string $data_string
	 * @return mixed
	 */
	public function unserialize($data_string);

}
// End of Core/Interfaces/Type.php